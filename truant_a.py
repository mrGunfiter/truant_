from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import QIcon, QPixmap, QColor
import admin_window, load_window
import sys, os
import datetime, time
import mysql.connector
from truant_tools import read_config, write_config, get_pc_name, connect_db, create_bd, load_school
from truant_classes import Grade, School, TruantException

programm_name = 'Truant'
version = '0.4'


class LoadWindow(QMainWindow, load_window.Ui_Load):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.window().setWindowTitle('%(name)s - %(version)s' % {'name': programm_name, 'version': version})
        self.setWindowIcon(QIcon('truant.ico'))
        self.lb_version.setText('<html><head/><body><p><span style=\"font-size:11pt; font-weight:600; color:#00007f;'
                                '\">{0}</span></p></body></html>'.format(version))
        self.le_pasword.setEchoMode(QLineEdit.Password)
        self.pb_create.clicked.connect(self.create_load)
        self.tb_file.clicked.connect(self.tb_file_click)
        # self.le_user.setText('admin')
        # self.le_pasword.setText('123456')
        # self.le_file.setText('sc1.csv')
        # self.le_server.setText('192.168.1.8')

    def create_load(self):
        if len(self.le_file.text()) == 0 or len(self.le_pasword.text()) == 0 or len(
                self.le_server.text()) == 0 or len(self.le_user.text()) == 0:
            answer = 'Заполните все поля ввода'
            QMessageBox.information(self, 'Внимание!', answer, QMessageBox.Ok)
        else:
            try:
                msg = QMessageBox()
                server = self.le_server.text()
                user = self.le_user.text()
                password = self.le_pasword.text()
                file = self.le_file.text()
                create_bd(user, password, 3306, server)
                load_school(file, user, password, 3306, server)
                answer = 'База успешно создана!\nПерезапустите программу'
                QMessageBox.information(self, 'Внимание!', answer, QMessageBox.Ok)
                config_list = dict(host=server, port='3306', user='teacher', dbname='truant', base='mysql', last_dig='1',
                                   last_let='А')
                write_config(config_list)
                self.pb_create.setDisabled(True)
                self.tb_file.setDisabled(True)
                self.le_user.setDisabled(True)
                self.le_pasword.setDisabled(True)
                self.le_file.setDisabled(True)
                self.le_server.setDisabled(True)
                exit(0)
            except (FileNotFoundError, ConnectionError, mysql.connector.Error, TruantException, Exception) as err:
                # print('XZ error: {}'.format(err))
                answer = 'Ошибка создания БД!\n\nТекст ошибки: %(err)s\n\nПроверте корректность вводимых данных, ' \
                         'полноту прав пользователя БД, удалите в БД базу truant и пользователя teacher, ' \
                         'затем попробуйте снова!' % {'err': err}
                QMessageBox.critical(msg, 'Ошибка!', answer, QMessageBox.Ok)
                exit(13)

    def tb_file_click(self):
        fname = QFileDialog.getOpenFileName(self, 'Open file', os.getcwd(), 'CSV files (*.csv) ')
        self.le_file.setText(fname[0])

    # Имитация загрузки данных
    @staticmethod
    def load_data(sp):
        for i in range(5):
            time.sleep(0.05)
            sp.showMessage("Загрузка    {0}%".format(i * 1), Qt.AlignHCenter | Qt.AlignCenter, Qt.darkBlue)
            qApp.processEvents()  # Запускаем оборот цикла
        time.sleep(1)


class AdminWindow(QMainWindow, admin_window.Ui_MainWindow):

    config_list = dict()
    conn = ''
    workdate = datetime.datetime.today()
    grade = Grade()
    school = School()
    all_grades = list()
    job_done = False

    def __init__(self, config_list, conn):
        super().__init__()
        self.setupUi(self)
        self.config_list = config_list
        self.window().setWindowTitle('%(name)s - %(version)s' % {'name': programm_name, 'version': version})
        self.setWindowIcon(QIcon('truant.ico'))
        self.grade.dig = self.config_list.get('last_dig')
        self.grade.let = self.config_list.get('last_let').upper()
        self.conn = conn
        if type(self.conn) == str:
            self.connected = False
        else:
            self.connected = self.conn.is_connected()
        self.cb_class_dig.setCurrentText(self.config_list.get('last_dig'))
        self.cb_class_let.setCurrentText(self.config_list.get('last_let').upper())
        self.lb_class.adjustSize()
        self.lb_class.adjustSize()
        if self.connected:
            self.lb_base.setText("<font color=\"#000000\">БД: OK</font>")
            self.read_bd_for_1class(self.config_list.get('last_dig'), self.config_list.get('last_let').upper())
            # ! Получить данные по всей школе
            self.read_bd_for_school()
        else:
            self.lb_base.setText("<font color=\"#ff0000\">БД: NO</font>")
            self.le_count_all_s.setText("")
        self.bt_save.setDisabled(not self.connected)
        self.bt_refresh.setDisabled(not self.connected)
        self.cb_class_dig.setDisabled(not self.connected)
        self.cb_class_let.setDisabled(not self.connected)
        self.le_count_all.setDisabled(not self.connected)
        self.de_showdate.calendarPopup()
        self.de_showdate.setDate(self.workdate.today())
        self.de_showdate.setMaximumDate(self.workdate.today())
        self.de_showdate.setDisabled(not self.connected)
        self.tb_view_all.setDisabled(not self.connected)
        self.te_truant.setDisabled(not self.connected)
        self.bt_refresh.setToolTip('Нажмите для <b>ОБНОВЛЕНИЯ</b> информации')
        self.bt_save.setToolTip('Нажмите для <b>СОХРАНЕНИЯ</b> информации по классу')
        self.fill_form()
        # слоты
        self.de_showdate.dateChanged.connect(self.change_date)
        self.sb_absent_ill.valueChanged.connect(self.sb_changed)
        self.sb_absent_other.valueChanged.connect(self.sb_changed)
        self.bt_refresh.clicked.connect(self.bt_refresh_clicked)
        self.bt_save.clicked.connect(self.bt_save_clicked)
        self.bt_about.clicked.connect(self.bt_about_clicked)
        self.cb_class_let.activated[str].connect(self.cb_class_let_clicked)
        self.le_class_now.setDisabled(not self.connected)
        self.le_school_now.setDisabled(not self.connected)
        self.tb_view_all.cellClicked.connect(self.cell_click)
        # self.tb_view_all.itemSelectionChanged.connect(self.cell_click)

    #
    def cell_click(self, row):
        dl = self.tb_view_all.item(row, 0).text().split('\"')
        self.cb_class_dig.setCurrentText(dl[0])
        self.cb_class_let.setCurrentText(dl[1])
        self.cb_class_let_clicked(self.cb_class_let.currentText())

    # Действие по изменению даты в календаре
    def change_date(self):
        self.workdate = datetime.datetime.strptime(self.de_showdate.date().toString('yyyy-MM-dd'), '%Y-%m-%d')
        self.read_bd_for_1class(self.grade.dig, self.grade.let)
        self.read_bd_for_school()
        self.fill_form()

    # Действие по нажатию кнопки "О Программе"
    def bt_about_clicked(self):
        text = 'в школе по классам\n\n(с) 2018 Черевко Егор\nМОБУ СОШ Агалатовский ЦО\n\npython 3.6.3 & PyQT5'
        QMessageBox.about(self, 'Truant %(ver)s - о программе' % { 'ver': version},
                          'Программа учета отсутствующих ' + text)

    # Действие по нажатию кнопки "Обновить"
    def bt_refresh_clicked(self):
        self.conn.close
        self.conn = connect_db(self.config_list)
        if type(self.conn) == str:
            QMessageBox.warning(self, 'Ошибка!', self.conn, QMessageBox.Ok)
            self.connected = False
        else:
            self.connected = self.conn.is_connected()
        self.read_bd_for_1class(self.grade.dig, self.grade.let)
        self.read_bd_for_school()
        self.fill_form()

    # есть ли изменения в экранной форме по одному классу
    def is_new(self):
        if (self.grade.ill == self.sb_absent_ill.value() and self.grade.other == self.sb_absent_other.value()
                and self.grade.dig == int(self.cb_class_dig.currentText())
                and self.grade.let == self.cb_class_let.currentText()
                and self.grade.comment == self.te_truant.toPlainText()):
            return False
        else:
            return True

    # Действие по нажатию кнопки "Сохранить"
    def bt_save_clicked(self):
        if int(self.le_count_all.text()) != self.grade.pupil:
            mess = 'Вы собираетесь изменить КОЛИЧЕСТВО УЧЕНИКОВ  в %(dig)s \"%(let)s\" классе! \n \n ' \
                   'ВЫ УВЕРЕНЫ?' % {'dig': self.grade.dig, 'let': self.grade.let}
            reply = QMessageBox.question(self, 'Вопрос', mess, QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
            if reply == QMessageBox.Yes:
                # Изменяем количество учеников в классе по списку
                self.change_pupil_bd()
        if self.is_new():
            mess = 'Записать данные об отсутствующих из \n %(dig)s \"%(let)s\" класса в БД? \n \n ' \
                   'ВЫ БЕРЁТЕ ОТВЕТСТВЕННОСТЬ ЗА ТОЧНОСТЬ ДАННЫХ?' % {'dig': self.grade.dig, 'let': self.grade.let}
            reply = QMessageBox.question(self, 'Вопрос', mess, QMessageBox.Yes | QMessageBox.No,  QMessageBox.No)
            if reply == QMessageBox.Yes:
                self.grade.ill = self.sb_absent_ill.value()
                self.grade.other = self.sb_absent_other.value()
                self.grade.absent = int(self.grade.ill) + int(self.grade.other)
                self.grade.pupil = int(self.le_count_all.text())
                self.grade.dig = self.cb_class_dig.currentText()
                self.grade.let = self.cb_class_let.currentText()
                self.grade.comment = self.te_truant.toPlainText()
                if int(self.grade.pupil) > int(self.grade.ill) + int(self.grade.other):
                    self.le_absent_all.setText(str(self.grade.absent))
                    if self.insert_bd():
                        self.job_done = True
                        answer = 'Данные об отсутствующих из \n %(dig)s \"%(let)s\" класса записаны в БД!' % {
                                 'dig': self.grade.dig,
                                 'let': self.grade.let}
                        QMessageBox.information(self, 'Внимание!', answer, QMessageBox.Ok)
                        self.fill_form()
                    else:
                        answer = 'Ошибка записи в БД! \n Возможно данные о %(dig)s \"%(let)s\" уже внесены в базу' % {
                                 'dig': self.grade.dig,
                                 'let': self.grade.let}
                        QMessageBox.warning(self, 'Ошибка!', answer, QMessageBox.Ok)
                        self.fill_form()
                else:
                    QMessageBox.information(self, 'Внимание', 'Ошибка записи в БД! \n Проверьте корректность данных!',
                                            QMessageBox.Ok)

    # Поиск значения grade gо id в self.all_grades и заполнения данных об отсутствующих
    def find_and_update(self, id, ill, other):
        found = False
        for i in range(len(self.all_grades)):
            if self.all_grades[i].id == id:
                self.all_grades[i].ill = ill
                self.all_grades[i].other = other
                self.all_grades[i].absent = ill + other
                self.all_grades[i].done = True
                found = True
                break
        return found

    # Получение данных из таблицы SCHOOL и TRUANTS по ВСЕЙ школе
    def read_bd_for_school(self):
        ok_query = 0
        flag_query = False
        self.all_grades = list()
        if self.conn.is_connected():
            # считаем учеников по списку в таблице SCHOOL
            cur = self.conn.cursor()
            sql_query = 'SELECT id, dig, let, pupil FROM school'
            cur.execute(sql_query)
            self.school.all = 0
            for id, dig, let, pupil in cur:
                grade = Grade()
                grade.id = id
                grade.dig = dig
                grade.let = let
                grade.pupil = pupil
                grade.absent = grade.ill + grade.other
                self.all_grades.append(grade)
                self.school.all += pupil
                ok_query += 1
            cur.close()
        # считаем прогульщиков на рабочую дату
            sql_query = ("""SELECT id, ill, other FROM truants WHERE date LIKE '%(date)s'"""
                          % {'date': self.workdate.strftime('%Y-%m-%d')})
            cur = self.conn.cursor()
            cur.execute(sql_query)
            self.school.ill = 0
            self.school.other = 0
            for id, ill, other in cur:
                self.find_and_update(id, ill, other)
                self.school.ill += ill
                self.school.other += other
                ok_query += 1
            cur.close()
            if ok_query > 0:
                flag_query = True
        return flag_query

    # Получение данных из таблицы SCHOOL и TRUANTS по одному классу школы
    def read_bd_for_1class(self, dig, let):
        flag_query = False
        if self.conn.is_connected():
            cur = self.conn.cursor()
            sql_query = ("""SELECT id, dig, let, pupil FROM school WHERE dig LIKE '%(dig)s' and let LIKE '%(let)s'""" %
                         {'dig': dig, 'let': let})
            cur.execute(sql_query)
            for (id, dig, let, pupil) in cur:
                self.grade.id = id
                self.grade.dig = dig
                self.grade.let = let
                self.grade.pupil = pupil
                flag_query = True
                self.job_done = False
            self.grade.ill = 0
            self.grade.other = 0
            self.grade.comment = ''
            if self.in_bd(self.grade.id, self.workdate.strftime('%Y-%m-%d')):
                sql_query = ("""SELECT ill, other, comment FROM truants 
                             WHERE id LIKE '%(id)s' and date LIKE '%(date)s'""" %
                             {'id': self.grade.id, 'date': self.workdate.strftime('%Y-%m-%d')})
                cur = self.conn.cursor()
                cur.execute(sql_query)
                for ill, other, comment in cur:
                    self.grade.ill = ill
                    self.grade.other = other
                    self.grade.comment = comment.decode('utf-8')
                self.job_done = True
            self.grade.absent = self.grade.ill + self.grade.other
        return flag_query

    # Проверка наличия данных в БД
    def in_bd(self, grade_id, date_now):
        if self.conn.is_connected():
            cur = self.conn.cursor()
            sql_query = ("""SELECT id, date FROM truants WHERE id LIKE '%(id)s' and date LIKE '%(date)s'""" %
                         {'id': grade_id, 'date': date_now})
            cur.execute(sql_query)
            cur.fetchall()
            if cur.rowcount <= 0:
                return False
            else:
                return True
        return False

    # изменение количества учеников по списку
    def change_pupil_bd(self):
        self.conn = connect_db(self.config_list)
        if self.conn.is_connected():
            try:
                old_pupil = self.grade.pupil
                self.grade.pupil = int(self.le_count_all.text())
                sql_query = ("""UPDATE school 
                                SET pupil = '%(pupil)s'
                                WHERE id LIKE '%(id)s'""" %
                             {'id': self.grade.id,
                              'pupil': self.grade.pupil})
                cur = self.conn.cursor()
                cur.execute(sql_query)
                self.conn.commit()
                return True
            except mysql.connector.Error as err:
                QMessageBox.information(self, 'Внимание', 'Ошибка БД: {}'.format(err), QMessageBox.Ok)
                self.grade.pupil = old_pupil
                return False

    # Добавление записи в БД по отсутствующим
    def insert_bd(self):
        # значения объекта grade записать в БД
        date_now = self.workdate.strftime('%Y-%m-%d')
        time_now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        self.conn = connect_db(self.config_list)
        if self.in_bd(self.grade.id, date_now):
            # обновляем существующую запись
            mess = 'Данные об отсутствующих \n %(dig)s \"%(let)s\" класса за %(date)s уже есть в БД!!! \n \n ' \
                   'ВЫ УВЕРЕНЫ?' % \
                   {'dig': self.grade.dig,
                    'let': self.grade.let,
                    'date': date_now}
            reply = QMessageBox.question(self, 'Вопрос', mess, QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
            if reply == QMessageBox.Yes:
                if self.conn.is_connected():
                    try:
                        sql_query = ("""UPDATE truants 
                                        SET ill = '%(ill)s', other = '%(other)s', comp = '%(comp)s', time = '%(time)s',
                                        comment = '%(comment)s'
                                        WHERE id LIKE '%(id)s' and date LIKE '%(date)s' """ %
                                     {'id': self.grade.id,
                                      'date': date_now,
                                      'ill': self.grade.ill,
                                      'other': self.grade.other,
                                      'comp': get_pc_name(),
                                      'time': time_now,
                                      'comment': self.grade.comment})
                        cur = self.conn.cursor()
                        cur.execute(sql_query)
                        self.conn.commit()
                        self.job_done = True
                        flag_query = True
                    except mysql.connector.Error as err:
                        QMessageBox.information(self, 'Внимание', 'Ошибка БД: {}'.format(err), QMessageBox.Ok)
                        self.job_done = False
                        flag_query = False
            else:
                self.job_done = True
                flag_query = False
        else:
            # добавляем новую запись
            if self.conn.is_connected():
                try:
                    sql_query = ("""INSERT INTO truants(id, date, ill, other, comp, time, comment)
                                VALUES ('%(id)s', '%(date)s', '%(ill)s', '%(other)s', '%(comp)s', '%(time)s', 
                                '%(comment)s' )""" %
                                 {'id': self.grade.id,
                                  'date': date_now,
                                  'ill': self.grade.ill,
                                  'other': self.grade.other,
                                  'comp': get_pc_name(),
                                  'time': time_now,
                                  'comment': self.grade.comment})
                    cur = self.conn.cursor()
                    cur.execute(sql_query)
                    self.conn.commit()
                    self.job_done = True
                    flag_query = True
                except mysql.connector.Error as err:
                    QMessageBox.information(self, 'Внимание', 'Ошибка БД: {}'.format(err), QMessageBox.Ok)
                    self.job_done = False
                    flag_query = False
            else:
                flag_query = False
        return flag_query

    # заполнение окна данными из БД
    def fill_form(self):
        self.le_count_all_s.setText(str(self.school.all))
        self.le_absent_other_s.setText(str(self.school.other))
        self.le_absent_ill_s.setText(str(self.school.ill))
        self.le_absent_all_s.setText(str(self.school.other + self.school.ill))
        self.cb_class_dig.setCurrentText(str(self.grade.dig))
        self.cb_class_let.setCurrentText(str(self.grade.let))
        self.le_count_all.setText(str(self.grade.pupil))
        self.le_class_now.setText(str(self.grade.pupil - self.grade.absent))
        self.le_school_now.setText(str(self.school.all - self.school.other - self.school.ill))
        self.te_truant.setText('')
        self.te_truant.setText(self.grade.comment)
        # self.te_truant.setAcceptRichText(False)
        if self.job_done:
            checked = '√'
        else:
            checked = ''
        self.lb_class.setText('%(dig)s\"%(let)s\" %(checked)s %(workdate)s'
                              % {'dig': self.grade.dig,
                                 'let': self.grade.let,
                                 'checked': checked,
                                 'workdate': self.workdate.strftime('%d.%m.%Y')})
        self.lb_class.adjustSize()
        self.sb_absent_ill.setValue(self.grade.ill)
        self.sb_absent_other.setValue(self.grade.other)
        self.sb_changed()
        self.enum_show()

    # действия по изменению количества отсутствующих
    def sb_changed(self):
        self.le_absent_all.setText(str(self.sb_absent_ill.value() + self.sb_absent_other.value()))
        self.sb_absent_ill.setMaximum(int(self.le_count_all.text()) - self.sb_absent_other.value())
        self.sb_absent_other.setMaximum(int(self.le_count_all.text()) - self.sb_absent_ill.value())
        if int(self.le_absent_all.text()) > 0:
            self.te_truant.setDisabled(False)
        else:
            self.te_truant.setDisabled(True)
            self.te_truant.setText('')

    # действия по выбору буквы класса
    def cb_class_let_clicked(self, text):
        self.conn.close
        self.conn = connect_db(self.config_list)
        if type(self.conn) == str:
            QMessageBox.warning(self, 'Ошибка!', self.conn, QMessageBox.Ok)
            self.connected = False
        else:
            self.connected = self.conn.is_connected()
        old_dig = self.grade.dig
        old_let = self.grade.let
        self.grade = Grade()
        self.grade.dig = int(self.cb_class_dig.currentText())
        self.grade.let = text
        if not self.read_bd_for_1class(self.grade.dig, self.grade.let):
            err = '%(dig)s \"%(let)s\" класса нет в БД!' % {'dig': self.grade.dig, 'let': self.grade.let}
            QMessageBox.warning(self, 'Ошибка!', err, QMessageBox.Ok)
            self.grade.let = old_let
            self.grade.dig = old_dig
            self.cb_class_dig.setCurrentText(str(self.grade.dig))
            self.cb_class_let.setCurrentText(self.grade.let)
            self.read_bd_for_1class(self.grade.dig, self.grade.let)
        self.fill_form()

    # Действия по нажатию на крестик закрытия окна
    def closeEvent(self, event):
        reply = QMessageBox.question(self, 'Вопрос', "Завершить работу?", QMessageBox.Yes | QMessageBox.No,
                                     QMessageBox.No)
        if reply == QMessageBox.Yes:
            self.config_list['last_dig'] = self.cb_class_dig.currentText()
            self.config_list['last_let'] = self.cb_class_let.currentText()
            write_config(self.config_list)
            event.accept()
        else:
            event.ignore()

    # Прорисовка списка классов
    def enum_show(self):
        self.tb_view_all.setColumnCount(6)  # Устанавливаем количество колонок
        self.tb_view_all.setRowCount(len(self.all_grades))  # и количество строк в таблице
        self.tb_view_all.setAlternatingRowColors(True)  # строки разного цвета через одну
        self.tb_view_all.setSelectionBehavior(1)  # выбор строки а не ячейки
        self.tb_view_all.verticalHeader().hide()

        # Устанавливаем заголовки таблицы
        self.tb_view_all.setHorizontalHeaderLabels(['Класс', 'Всего', 'Нет', 'Бол.', 'Др.', 'Здесь'])

        # Устанавливаем всплывающие подсказки на заголовки
        self.tb_view_all.horizontalHeaderItem(0).setToolTip('Класс ')
        self.tb_view_all.horizontalHeaderItem(1).setToolTip('Всего по списку ')
        self.tb_view_all.horizontalHeaderItem(2).setToolTip('Всего отсутствуют ')
        self.tb_view_all.horizontalHeaderItem(3).setToolTip('Отсутствуют по болезни ')
        self.tb_view_all.horizontalHeaderItem(4).setToolTip('Отсутствуют по другой причине ')
        self.tb_view_all.horizontalHeaderItem(5).setToolTip('Присутствуют ')

        # Устанавливаем выравнивание на заголовки
        self.tb_view_all.horizontalHeaderItem(0).setTextAlignment(Qt.AlignCenter)
        self.tb_view_all.horizontalHeaderItem(1).setTextAlignment(Qt.AlignCenter)
        self.tb_view_all.horizontalHeaderItem(2).setTextAlignment(Qt.AlignCenter)
        self.tb_view_all.horizontalHeaderItem(3).setTextAlignment(Qt.AlignCenter)
        self.tb_view_all.horizontalHeaderItem(4).setTextAlignment(Qt.AlignCenter)
        self.tb_view_all.horizontalHeaderItem(5).setTextAlignment(Qt.AlignLeft)
        self.tb_view_all.horizontalHeader().setStretchLastSection(True)

        for i in range(len(self.all_grades)):
            # заполняем таблицу данными
            self.tb_view_all.setItem(i, 0, QTableWidgetItem(str(self.all_grades[i].dig) + '\"'
                                                            + self.all_grades[i].let + '\"'))
            self.tb_view_all.setItem(i, 1, QTableWidgetItem(str(self.all_grades[i].pupil)))
            self.tb_view_all.setItem(i, 2, QTableWidgetItem(str(self.all_grades[i].absent)))
            self.tb_view_all.setItem(i, 3, QTableWidgetItem(str(self.all_grades[i].ill)))
            self.tb_view_all.setItem(i, 4, QTableWidgetItem(str(self.all_grades[i].other)))
            self.tb_view_all.setItem(i, 5, QTableWidgetItem(str(self.all_grades[i].pupil - self.all_grades[i].absent)))
            # раскрашиваем строки в зависимости от содержимого
            if self.all_grades[i].done is False:
                for j in range(self.tb_view_all.columnCount()):
                    self.tb_view_all.item(i, j).setBackground(QColor(255, 0, 0, 127))
            if self.all_grades[i].done is True:
                for j in range(self.tb_view_all.columnCount()):
                    self.tb_view_all.item(i, j).setBackground(QColor(0, 255, 0, 127))

        # делаем ресайз колонок по содержимому
        self.tb_view_all.resizeColumnsToContents()
        self.tb_view_all.verticalHeader().setDefaultSectionSize(self.tb_view_all.verticalHeader().minimumSectionSize())

    # Имитация загрузки данных
    @staticmethod
    def load_data(sp):
        for i in range(100):
            time.sleep(0.025 )
            sp.showMessage("Загрузка    {0}%".format(i * 1), Qt.AlignHCenter | Qt.AlignCenter, Qt.darkBlue)
            qApp.processEvents()  # Запускаем оборот цикла


if __name__ == '__main__':
    app = QApplication(sys.argv)
    splash = QSplashScreen(QPixmap('truant.jpg') )
    splash.showMessage("Загрузка    0%", Qt.AlignHCenter | Qt.AlignCenter, Qt.darkBlue)
    splash.show()  # Отображаем заставку
    qApp.processEvents()  # Запускаем оборот цикла
    translator = QTranslator()
    translator.load(u'qt_ru', u'')  # расширение файла (.qm) Qt сам добавит.
    app.installTranslator(translator)
    try:
        msg = QMessageBox(splash)
        config_list = read_config()
        if config_list.get('host') == 'server':
            loadform = LoadWindow()
            loadform.load_data(splash)
            reply = QMessageBox.critical(msg, 'Ошибка!', 'БД не найдена! Это первый запуск программы?',
                                         QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
            if reply == QMessageBox.Yes:
                loadform.show()
                splash.finish(loadform)
                app.exec_()
            else:
                answer = 'Неверно указаны данные в файле настроек TRUANT.INI'
                QMessageBox.critical(msg, 'Ошибка!', answer, QMessageBox.Ok)
                exit(0)
        conn = connect_db(config_list)
        form = AdminWindow(config_list, conn)
        form.load_data(splash)
        form.show()
        splash.finish(form)
        app.exec_()
    except FileNotFoundError:
        QMessageBox.critical(msg, 'Ошибка!', 'Файд настроек TRUANT.INI не найден! Обратитесь к администратору!',
                             QMessageBox.Ok)
    except (ConnectionError, mysql.connector.Error, TruantException) as err:
        QMessageBox.critical(msg, 'Ошибка!', 'Ошибка подключения к серверу Базы Данных!\n\n'
                                             'СЕРВЕР: %(server)s ПОРТ: %(port)s USER: %(user)s\n\n'
                                             'Текст ошибки: %(err)s\n\n'
                                             'Обратитесь к администратору!'
                             % {'server': config_list.get('host'), 'port': config_list.get('port'),
                                'user': config_list.get('user'), 'err': err}, QMessageBox.Ok)

