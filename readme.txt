# README #

Проект TRUANT

Программа "TRUANT" для контроля отсутствующих на уроках.

Язык разработки: python 3.6.3
GUI: PyQT5, QtCreator 
Base: MySQL, mysql.connector 
EXE: cx_Freeze


truant_a.py        - отчетный модуль 
truant_t.py        - учительский модуль
admin_window.py    - окно отчетного модуля
load_window.py     - окно ввода первоначальных данных для подключения к MySQL
teach_window.py    - окно учительского модуля
make_exe_a.py      - создание truant_a.exe
make_exe_t.py      - создание truant_t.exe
truant_classes.py  - описание классов используемых в программе
truant_tools.py    - вспомогательные функции 