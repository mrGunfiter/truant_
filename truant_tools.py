import socket
import mysql.connector
from truant_classes import TruantException


# функция чтения параметров настройки программы из truant.ini
# если файла нет, то он создается и выводится диалоговое окно для ввода адреса сервера,
# адрес сервера записывается в truant.ini, в первой строке, строка может быть не обязательно первой:
# server=192.168.1.1
# или
# server=MyCoolSuperServer
def read_config():
    config_list = dict(host='server', port='port', user='user', dbname='basename', base='mysql')
    try:
        file = open('truant.ini', 'r', encoding='UTF-8')
        for line in file.readlines():
            if line.strip().__len__() == 0:
                continue
            line = line.split('=')
            line = [line.rstrip() for line in line]
            config_list[line[0].lower()] = line[1].lower()
        return config_list
    except FileNotFoundError as err:
        raise FileNotFoundError(err)


# запись файла конигурации
# данные словаря config_list записать в файл truant.ini, переписав файл заново
def write_config(config_list):
    file = open('truant.ini', 'w', encoding='UTF-8')
    promez = []
    for key, value in config_list.items():
        promez.append(key + '=' + str(value) + '\n')
    file.writelines(promez)
    file.close()
    return True


# получение имени компьютера
def get_pc_name():
    if socket.gethostname().find('.') >= 0:
        name = socket.gethostname()
    else:
        name = socket.gethostbyaddr(socket.gethostname())[0]
    return name


def connect_db(config_list):
        try:
            conn = mysql.connector.connect(user=config_list.get('user'),
                                           password='p@ss2018',
                                           host=config_list.get('host'),
                                           database=config_list.get('dbname'),
                                           port=config_list.get('port'))
            return conn
        except mysql.connector.Error as err:
            raise ConnectionError(err)
            # return 'Connection error: {}'.format(err)
        except Exception as err:
            raise TruantException(err)
            # return 'Connection error: {}'.format(err)


# создание БД при первом запуске
def create_bd(user, password, port, host):
    try:
        conn = mysql.connector.connect(user=user,
                                       password=password,
                                       host=host,
                                       port=port)
        sql_query = ("""CREATE DATABASE `truant` /*!40100 DEFAULT CHARACTER SET utf8 */;
                        
                        USE truant;
                        
                        CREATE TABLE `school` (
                          `id` int(11) NOT NULL AUTO_INCREMENT,
                          `dig` tinyint(1) NOT NULL,
                          `let` varchar(1) CHARACTER SET utf8 NOT NULL,
                          `pupil` tinyint(50) NOT NULL,
                          PRIMARY KEY (`id`)
                        ) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
                        
                        CREATE TABLE `truants` (
                          `id` int(11) NOT NULL,
                          `date` date NOT NULL,
                          `ill` tinyint(50) DEFAULT NULL,
                          `other` tinyint(50) DEFAULT NULL,
                          `comp` varchar(45) CHARACTER SET big5 COLLATE big5_bin NOT NULL,
                          `time` datetime(1) NOT NULL,
                          `comment` varchar(255) COLLATE utf8_bin DEFAULT NULL,
                          PRIMARY KEY (`id`,`date`),
                          KEY `id_idx` (`id`),
                          CONSTRAINT `id` FOREIGN KEY (`id`) REFERENCES `school` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
                        
                        CREATE USER 'teacher'@'%' IDENTIFIED BY 'p@ss2018';
                        GRANT ALL PRIVILEGES ON `truant`.* TO 'teacher'@'%';
                        FLUSH PRIVILEGES;""")
        cur = conn.cursor()
        for result in cur.execute(sql_query, multi=True):
            pass
        conn.commit()
    except mysql.connector.Error as err:
        # print('Connection error: {}'.format(err))
        raise ConnectionError(err)
        # return 'Connection error: {}'.format(err)
    except Exception as err:
        # print('Exception error: {}'.format(err))
        raise TruantException(err)
        # return 'Connection error: {}'.format(err)


# распаковка строки, в которой поля записаны с разделителем ";"
def unpack_line(line):
    line = line.replace('\'', '')
    line = line.split(';')
    dig = line[0]
    let = line[1]
    pupil = line[2]
    return dig, let, pupil


# загрузка csv-файла со списком классов в БД
def load_school(csvname, user, password, port, host):
    try:
        # подключаемся к базе данных
        conn = mysql.connector.connect(user=user,
                                       password=password,
                                       database='truant',
                                       host=host,
                                       port=port)
        # формируем курсор, с помощью которого можно исполнять SQL-запросы
        cur = conn.cursor()
        # открываем исходный csv-файл
        # f = open(csvname, "r", encoding='UTF-8')
        f = open(csvname, "r", encoding='UTF-8')
        # представляем его в виде массива строк
        lines = f.readlines()
        for line in lines:
            # извлекаем данные из строки
            dig, let, pupil = unpack_line(line)
            if not dig.isdigit():
                continue
            dig = int(dig)
            pupil = int(pupil)
            # подставляем эти данные в SQL-запрос
            sql_query = """INSERT INTO school(dig, let, pupil)
                           VALUES ('%(dig)s', '%(let)s', '%(pupil)s')
                        """ % {"dig": dig, "let": let, "pupil": pupil}
            # исполняем SQL-запрос
            # print(sql_query)
            cur.execute(sql_query)
            # применяем изменения к базе данных
            conn.commit()
        # закрываем соединение с базой данных
        conn.close()
        # закрываем файл
        f.close()
    except FileNotFoundError as err:
        # print('FileNotFound error: {}'.format(err))
        raise FileNotFoundError(err)
    except mysql.connector.Error as err:
        # print('Connection error: {}'.format(err))
        raise ConnectionError(err)
    except Exception as err:
        # print('Exception error: {}'.format(err))
        raise TruantException(err)
    return True


if __name__ == "__main__":
    # print(create_bd('admin', 'Gthcgtrnbdf128', '3306', '192.168.23.90'))
    load_school('sc1.csv', 'admin', '123456', '3306', '192.168.1.8')


