# coding: utf-8
# python make_exe.py build

import sys
from cx_Freeze import setup, Executable

# base = 'None'
# if sys.platform == 'win32':
base = 'Win32GUI'
# elif sys.platform == 'win64':
#     base = 'Win64GUI'

executables = [Executable('truant_t.py', 
                          targetName='truant_t.exe', 
                          base=base,
                          icon='truant.ico')]

excludes = ['unicodedata', 'logging', 'unittest', 'email', 'html', 'http', 'urllib',
           'xml', 'bz2']
# excludes = ['unicodedata', 'logging', 'unittest', 'email', 'html', 'http', 'urllib',
#            'xml', 'pydoc', 'doctest', 'argparse', 'datetime', 'zipfile',
#            'subprocess', 'pickle', 'threading', 'locale', 'calendar', 
#            'base64', 'gettext', 'bz2', 'fnmatch', 'getopt', 'string', 'stringprep',
#            'quopri', 'copy']

# includes = ['json']

zip_include_packages = ['collections', 'encodings', 'importlib', 
                        'PyQt5', 'imp', 'functools', 'operator', 
                        'keyword', 'heapq', 'reprlib', 'weakref',
                        'contextlib', 'tokenize', 're']

options = {
    'build_exe': {
        'include_msvcr': True,
        # 'excludes': excludes,
        # 'includes': includes,
        'zip_include_packages': zip_include_packages,
        'build_exe': 'truant_t',
    }
}

setup(name='TRUANT_T',
      version='0.4',
      description='TRUANT_T',
      executables=executables,
      options=options)