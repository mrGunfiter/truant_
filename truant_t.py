# from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import QIcon, QPixmap
import teach_window
import sys
import datetime, time
import mysql.connector
from truant_tools import read_config, write_config, get_pc_name, connect_db
from truant_classes import Grade, TruantException

programm_name = 'Truant'
version = '0.4'


class TeachWindow(QMainWindow, teach_window.Ui_MainWindow):

    config_list = dict()
    conn = ''
    grade = Grade()
    job_done = False

    def __init__(self, config_list, conn):
        super().__init__()
        self.setupUi(self)
        self.window().setWindowTitle('%(name)s - %(version)s' % {'name': programm_name, 'version': version})
        self.setWindowIcon(QIcon('truant.ico'))
        self.config_list = config_list
        self.grade.dig = self.config_list.get('last_dig')
        self.grade.let = self.config_list.get('last_let').upper()
        self.conn = conn
        if type(self.conn) == str:
            self.connected = False
        else:
            self.connected = self.conn.is_connected()
        self.cb_class_dig.setCurrentText(self.config_list.get('last_dig'))
        self.cb_class_let.setCurrentText(self.config_list.get('last_let').upper())
        self.lb_class.adjustSize()
        self.le_count_all.setDisabled(True)
        self.le_absent_all.setDisabled(True)
        self.le_now_here.setDisabled(True)
        if self.connected:
            self.lb_base.setText("<font color=\"#000000\">БД: OK</font>")
            self.read_db(self.config_list.get('last_dig'), self.config_list.get('last_let').upper())
        else:
            self.lb_base.setText("<font color=\"#ff0000\">БД: NO</font>")
            self.le_count_all.setText('')
        self.fill_form()
        self.bt_go.setDisabled(not self.connected)
        self.cb_class_dig.setDisabled(not self.connected)
        self.cb_class_let.setDisabled(not self.connected)
        self.sb_absent_ill.setDisabled(not self.connected)
        self.sb_absent_other.setDisabled(not self.connected)
        # слоты
        self.bt_about.clicked.connect(self.bt_about_clicked)
        self.bt_go.clicked.connect(self.bt_go_clicked)
        self.sb_absent_ill.valueChanged.connect(self.sd_changed)
        self.sb_absent_other.valueChanged.connect(self.sd_changed)
        self.cb_class_let.activated[str].connect(self.cb_class_let_clicked)

    # Получение данных из таблиц SCHOOL и TRUANTS по одному классу школы
    def read_db(self, dig, let):
        flag_query = False
        if self.conn.is_connected():
            cur = self.conn.cursor()
            sql_query = ("""SELECT id, dig, let, pupil FROM school WHERE dig LIKE '%(dig)s' and let LIKE '%(let)s'""" %
                         {'dig': dig, 'let': let})
            cur.execute(sql_query)
            for (id, dig, let, pupil) in cur:
                self.grade.id = id
                self.grade.dig = dig
                self.grade.let = let
                self.grade.pupil = pupil
                flag_query = True
                self.job_done = False
            self.grade.ill = 0
            self.grade.other = 0
            self.grade.comment = ''
            if self.in_bd(self.grade.id, datetime.datetime.today().strftime('%Y-%m-%d')):
                sql_query = ("""SELECT ill, other, comment FROM truants 
                             WHERE id LIKE '%(id)s' and date LIKE '%(date)s'""" %
                             {'id': self.grade.id, 'date': datetime.datetime.today().strftime('%Y-%m-%d')})
                cur = self.conn.cursor()
                cur.execute(sql_query)
                for ill, other, comment in cur:
                    self.grade.ill = int(ill)
                    self.grade.other = int(other)
                    self.grade.comment = comment.decode('utf-8')
                self.job_done = True
        return flag_query

    # Проверка наличия данных в БД
    def in_bd(self, grade_id, date_now):
        if self.conn.is_connected():
            cur = self.conn.cursor()
            sql_query = ("""SELECT id, date FROM truants WHERE id LIKE '%(id)s' and date LIKE '%(date)s'""" %
                         {'id': grade_id, 'date': date_now})
            cur.execute(sql_query)
            cur.fetchall()
            if cur.rowcount <= 0:
                return False
            else:
                return True
        return False

    # Добавление записи в БД
    def insert_bd(self):
        # значения объекта grade записать в БД
        date_now = datetime.datetime.today().strftime('%Y-%m-%d')
        time_now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        self.conn = connect_db(self.config_list)
        if self.in_bd(self.grade.id, date_now):
            # обновляем существующую запись
            mess = 'Данные об отсутствующих \n %(dig)s \"%(let)s\" класса за %(date)s уже есть в БД!!! \n \n ' \
                   'ВЫ УВЕРЕНЫ?' % \
                   {'dig': self.grade.dig,
                    'let': self.grade.let,
                    'date': date_now}
            reply = QMessageBox.question(self, 'Вопрос', mess, QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
            if reply == QMessageBox.Yes:
                if self.conn.is_connected():
                    try:
                        sql_query = ("""UPDATE truants 
                                        SET ill = '%(ill)s', other = '%(other)s', comp = '%(comp)s', time = '%(time)s',
                                        comment = '%(comment)s'
                                        WHERE id LIKE '%(id)s' and date LIKE '%(date)s' """ %
                                     {'id': self.grade.id,
                                      'date': date_now,
                                      'ill': self.grade.ill,
                                      'other': self.grade.other,
                                      'comp': get_pc_name(),
                                      'time': time_now,
                                      'comment': self.grade.comment})
                        cur = self.conn.cursor()
                        cur.execute(sql_query)
                        self.conn.commit()
                        self.job_done = True
                        flag_query = True
                    except mysql.connector.Error as err:
                        QMessageBox.information(self, 'Внимание', 'Ошибка БД: {}'.format(err), QMessageBox.Ok)
                        self.job_done = False
                        flag_query = False
            else:
                self.job_done = True
                flag_query = False
        else:
            # добавляем новую запись
            if self.conn.is_connected():
                try:
                    sql_query = ("""INSERT INTO truants(id, date, ill, other, comp, time, comment)
                                VALUES ('%(id)s', '%(date)s', '%(ill)s', '%(other)s', '%(comp)s', '%(time)s', 
                                '%(comment)s' )""" %
                                 {'id': self.grade.id,
                                  'date': date_now,
                                  'ill': self.grade.ill,
                                  'other': self.grade.other,
                                  'comp': get_pc_name(),
                                  'time': time_now,
                                  'comment': self.grade.comment})
                    cur = self.conn.cursor()
                    cur.execute(sql_query)
                    self.conn.commit()
                    self.job_done = True
                    flag_query = True
                except mysql.connector.Error as err:
                    QMessageBox.information(self, 'Внимание', 'Ошибка БД: {}'.format(err), QMessageBox.Ok)
                    self.job_done = False
                    flag_query = False
            else:
                flag_query = False
        return flag_query

    # заполнение окна данными из БД
    def fill_form(self):
        self.cb_class_dig.setCurrentText(str(self.grade.dig))
        self.cb_class_let.setCurrentText(str(self.grade.let))
        self.le_count_all.setText(str(self.grade.pupil))
        self.te_truant.setText(self.grade.comment)
        if self.job_done:
            checked = '√'
        else:
            checked = ''
        self.lb_class.setText('%(dig)s\"%(let)s\" %(checked)s' % {'dig': self.grade.dig, 'let': self.grade.let,
                                                                  'checked': checked})
        self.lb_class.adjustSize()
        self.sb_absent_ill.setValue(self.grade.ill)
        self.sb_absent_other.setValue(self.grade.other)
        self.sd_changed()

    # действия по изменению количества отсутствующих
    def sd_changed(self):
        self.le_absent_all.setText(str(self.sb_absent_ill.value() + self.sb_absent_other.value()))
        self.le_now_here.setText(str(int(self.le_count_all.text()) -
                                     (self.sb_absent_other.value() + self.sb_absent_ill.value())))
        self.sb_absent_ill.setMaximum(int(self.le_count_all.text()) - self.sb_absent_other.value())
        self.sb_absent_other.setMaximum(int(self.le_count_all.text()) - self.sb_absent_ill.value())
        if int(self.le_absent_all.text()) > 0:
            self.te_truant.setDisabled(False)
        else:
            self.te_truant.setDisabled(True)
            self.te_truant.setText('')

    # действия по выбору буквы класса
    def cb_class_let_clicked(self, text):
        self.conn.close
        self.conn = connect_db(self.config_list)
        if type(self.conn) == str:
            QMessageBox.warning(self, 'Ошибка!', self.conn, QMessageBox.Ok)
            self.connected = False
        else:
            self.connected = self.conn.is_connected()
        old_dig = self.grade.dig
        old_let = self.grade.let
        self.grade = Grade()
        self.grade.dig = int(self.cb_class_dig.currentText())
        self.grade.let = text
        if not self.read_db(self.grade.dig, self.grade.let):
            err = '%(dig)s \"%(let)s\" класса нет в БД!' % {'dig': self.grade.dig, 'let': self.grade.let}
            QMessageBox.warning(self, 'Ошибка!', err, QMessageBox.Ok)
            self.grade.let = old_let
            self.grade.dig = old_dig
            self.cb_class_dig.setCurrentText(str(self.grade.dig))
            self.cb_class_let.setCurrentText(self.grade.let)
            self.read_db(self.grade.dig, self.grade.let)
        self.fill_form()

    # Действие по нажатию кнопки "О Программе"
    def bt_about_clicked(self):
        text = 'в школе по классам\n\n(с) 2018 Черевко Егор\nМОБУ СОШ Агалатовский ЦО\n\npython 3.6.3 & PyQT5'
        QMessageBox.about(self, 'Truant %(ver)s - о программе' % {'ver': version},
                          'Программа учета отсутствующих\n' + text)

    # действия по нажатии кнопки "Отправить"
    def bt_go_clicked(self):
        mess = 'Записать данные об отсутствующих из \n %(dig)s \"%(let)s\" класса в БД? \n \n ' \
               'ВЫ БЕРЁТЕ ОТВЕТСТВЕННОСТЬ ЗА ТОЧНОСТЬ ДАННЫХ?' % {'dig': self.grade.dig, 'let': self.grade.let}
        reply = QMessageBox.question(self, 'Вопрос', mess, QMessageBox.Yes | QMessageBox.No,  QMessageBox.No)
        if reply == QMessageBox.Yes:
            self.grade.ill = self.sb_absent_ill.value()
            self.grade.other = self.sb_absent_other.value()
            self.grade.absent = int(self.grade.ill) + int(self.grade.other)
            self.grade.pupil = self.le_count_all.text()
            self.grade.dig = self.cb_class_dig.currentText()
            self.grade.let = self.cb_class_let.currentText()
            self.grade.comment = self.te_truant.toPlainText()
            if int(self.grade.pupil) > int(self.grade.ill) + int(self.grade.other):
                self.le_absent_all.setText(str(self.grade.absent))
                self.le_now_here.setText(str(int(self.grade.pupil) - self.grade.absent))
                if self.insert_bd():
                    self.job_done = True
                    answer = 'Данные об отсутствующих из \n %(dig)s \"%(let)s\" класса записаны в БД!' % {
                             'dig': self.grade.dig,
                             'let': self.grade.let}
                    QMessageBox.information(self, 'Внимание!', answer, QMessageBox.Ok)
                    self.fill_form()
                else:
                    answer = 'Ошибка записи в БД! \n Возможно данные о %(dig)s \"%(let)s\" уже внесены в базу' % {
                             'dig': self.grade.dig,
                             'let': self.grade.let}
                    QMessageBox.warning(self, 'Ошибка!', answer, QMessageBox.Ok)
                    self.fill_form()
            else:
                QMessageBox.information(self, 'Внимание', 'Ошибка записи в БД! \n Проверьте корректность данных!',
                                        QMessageBox.Ok)

    # Действия по нажатию на крестик закрытия окна
    def closeEvent(self, event):
        reply = QMessageBox.question(self, 'Вопрос', 'Завершить работу?', QMessageBox.Yes | QMessageBox.No,
                                     QMessageBox.No)
        if reply == QMessageBox.Yes:
            self.config_list['last_dig'] = self.cb_class_dig.currentText()
            self.config_list['last_let'] = self.cb_class_let.currentText()
            write_config(self.config_list )
            event.accept()
        else:
            event.ignore()

    #  Имитация загрузки данных
    @staticmethod
    def load_data(sp):
        for i in range(100):
            time.sleep(0.025)
            sp.showMessage("Загрузка    {0}%".format(i * 1), Qt.AlignHCenter | Qt.AlignCenter, Qt.darkBlue)
            qApp.processEvents()  # Запускаем оборот цикла


if __name__ == '__main__':
    app = QApplication(sys.argv)
    splash = QSplashScreen(QPixmap('truant.jpg'))
    splash.showMessage("Загрузка    0%", Qt.AlignHCenter | Qt.AlignCenter, Qt.darkBlue)
    splash.show()  # Отображаем заставку
    qApp.processEvents()  # Запускаем оборот цикла
    translator = QTranslator()
    translator.load(u'qt_ru', u'')  # расширение файла (.qm) Qt сам добавит.
    app.installTranslator(translator)
    try:
        msg = QMessageBox(splash)
        config_list = read_config()
        conn = connect_db(config_list)
        form = TeachWindow(config_list, conn)
        form.load_data(splash)
        form.show()
        splash.finish(form)
        app.exec()
    except FileNotFoundError:
        QMessageBox.critical(msg, 'Ошибка!', 'Файд настроек TRUANT.INI не найден! Обратитесь к администратору!',
                             QMessageBox.Ok)
    except (ConnectionError, TruantException) as err:
        QMessageBox.critical(msg, 'Ошибка!', 'Ошибка подключения к серверу Базы Данных!\n\n'
                                             'СЕРВЕР: %(server)s ПОРТ: %(port)s USER: %(user)s\n\n'
                                             'Текст ошибки: %(err)s\n\n'
                                             'Обратитесь к администратору!'
                             % {'server': config_list.get('host'), 'port': config_list.get('port'),
                                'user': config_list.get('user'), 'err': err}, QMessageBox.Ok)
