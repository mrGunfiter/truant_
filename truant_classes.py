"""
Классы используемые в программе
"""


class TruantException(Exception):
    pass


# класс для описания всей школы:
# all    - всего по списку
# ill    - отсутствуют по болезни
# other  - отсутствуют по другим причинам
class School:
    def __init__(self):
        self._all = 0
        self._ill = 0
        self._other = 0

    @property
    def all(self):
        return self._all

    @all.setter
    def all(self, _all):
        self._all = int(_all)

    @property
    def ill(self):
        return self._ill

    @ill.setter
    def ill(self, _ill):
        self._ill = int(_ill)

    @property
    def other(self):
        return self._other

    @other.setter
    def other(self, _other):
        self._other = int(_other)


# класс для описания класса школы:
# id      - уникальный идентификатор в БД
# dig     - буква класса
# let     - цифра класса
# pupil   - кол-во по списку
# absent  - всего отсутствуют absent = ill + other
# ill     - отсутствуют по болезни
# other   - отсутствуют по другим причинам
# comment - примечание, на первом этапе храним там ФИО прогульщиков
# done    - данные по остутствующим заполнены на дату
class Grade:
    def __init__(self):
        self._id = 0
        self._dig = 1
        self._let = 'А'
        self._pupil = 0
        self._absent = 0
        self._ill = 0
        self._other = 0
        self._comment = ''
        self._done = False

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, _id):
        self._id = int(_id)

    @property
    def dig(self):
        return self._dig

    @dig.setter
    def dig(self, _dig):
        self._dig = int(_dig)

    @property
    def let(self):
        return self._let

    @let.setter
    def let(self, _let):
        self._let = str(_let)

    @property
    def pupil(self):
        return self._pupil

    @pupil.setter
    def pupil(self, _pupil):
        self._pupil = int(_pupil)

    @property
    def absent(self):
        return self._absent

    @absent.setter
    def absent(self, _absent):
        self._absent = int(_absent)

    @property
    def ill(self):
        return self._ill

    @ill.setter
    def ill(self, _ill):
        self._ill = int(_ill)

    @property
    def other(self):
        return self._other

    @other.setter
    def other(self, _other):
        self._other = int(_other)

    @property
    def comment(self):
        return self._comment

    @comment.setter
    def comment(self, _comment):
        self._comment = str(_comment)

    @property
    def done(self):
        return self._done

    @done.setter
    def done(self, _done):
        self._done = _done

    def info(self):
        print(self.dig)
        print(self.let)
        print(self.pupil)


if __name__ == "__main__":
    grade = Grade()
    school = School()
    print(grade.info())