# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'load.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Load(object):
    def setupUi(self, Load):
        Load.setObjectName("Load")
        Load.resize(292, 254)
        Load.setMinimumSize(QtCore.QSize(292, 254))
        Load.setMaximumSize(QtCore.QSize(292, 254))
        self.centralWidget = QtWidgets.QWidget(Load)
        self.centralWidget.setObjectName("centralWidget")
        self.lb_server = QtWidgets.QLabel(self.centralWidget)
        self.lb_server.setGeometry(QtCore.QRect(14, 91, 41, 16))
        self.lb_server.setObjectName("lb_server")
        self.lb_user = QtWidgets.QLabel(self.centralWidget)
        self.lb_user.setGeometry(QtCore.QRect(14, 117, 109, 16))
        self.lb_user.setObjectName("lb_user")
        self.lb_pasword = QtWidgets.QLabel(self.centralWidget)
        self.lb_pasword.setGeometry(QtCore.QRect(14, 144, 41, 16))
        self.lb_pasword.setObjectName("lb_pasword")
        self.lb_file = QtWidgets.QLabel(self.centralWidget)
        self.lb_file.setGeometry(QtCore.QRect(14, 170, 30, 16))
        self.lb_file.setObjectName("lb_file")
        self.pb_create = QtWidgets.QPushButton(self.centralWidget)
        self.pb_create.setGeometry(QtCore.QRect(200, 220, 75, 23))
        self.pb_create.setObjectName("pb_create")
        self.le_server = QtWidgets.QLineEdit(self.centralWidget)
        self.le_server.setGeometry(QtCore.QRect(130, 91, 153, 20))
        self.le_server.setObjectName("le_server")
        self.le_user = QtWidgets.QLineEdit(self.centralWidget)
        self.le_user.setGeometry(QtCore.QRect(130, 117, 153, 20))
        self.le_user.setObjectName("le_user")
        self.le_pasword = QtWidgets.QLineEdit(self.centralWidget)
        self.le_pasword.setGeometry(QtCore.QRect(130, 143, 153, 20))
        self.le_pasword.setObjectName("le_pasword")
        self.lb_truant = QtWidgets.QLabel(self.centralWidget)
        self.lb_truant.setGeometry(QtCore.QRect(22, 220, 47, 21))
        self.lb_truant.setObjectName("lb_truant")
        self.lb_version = QtWidgets.QLabel(self.centralWidget)
        self.lb_version.setGeometry(QtCore.QRect(107, 222, 31, 20))
        self.lb_version.setObjectName("lb_version")
        self.label = QtWidgets.QLabel(self.centralWidget)
        self.label.setGeometry(QtCore.QRect(20, 10, 261, 61))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(False)
        font.setWeight(50)
        self.label.setFont(font)
        self.label.setTextFormat(QtCore.Qt.RichText)
        self.label.setWordWrap(True)
        self.label.setObjectName("label")
        self.ln_top = QtWidgets.QFrame(self.centralWidget)
        self.ln_top.setGeometry(QtCore.QRect(2, 70, 290, 16))
        self.ln_top.setFrameShape(QtWidgets.QFrame.HLine)
        self.ln_top.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.ln_top.setObjectName("ln_top")
        self.ln_bottom = QtWidgets.QFrame(self.centralWidget)
        self.ln_bottom.setGeometry(QtCore.QRect(0, 200, 290, 16))
        self.ln_bottom.setFrameShape(QtWidgets.QFrame.HLine)
        self.ln_bottom.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.ln_bottom.setObjectName("ln_bottom")
        self.le_file = QtWidgets.QLineEdit(self.centralWidget)
        self.le_file.setGeometry(QtCore.QRect(130, 169, 130, 20))
        self.le_file.setObjectName("le_file")
        self.tb_file = QtWidgets.QToolButton(self.centralWidget)
        self.tb_file.setGeometry(QtCore.QRect(260, 169, 25, 20))
        self.tb_file.setObjectName("tb_file")
        Load.setCentralWidget(self.centralWidget)

        self.retranslateUi(Load)
        QtCore.QMetaObject.connectSlotsByName(Load)

    def retranslateUi(self, Load):
        _translate = QtCore.QCoreApplication.translate
        Load.setWindowTitle(_translate("Load", "Load"))
        self.lb_server.setText(_translate("Load", "Сервер:"))
        self.lb_user.setText(_translate("Load", "Супер пользователь:"))
        self.lb_pasword.setText(_translate("Load", "Пароль:"))
        self.lb_file.setText(_translate("Load", "Файл:"))
        self.pb_create.setText(_translate("Load", "Создать"))
        self.lb_truant.setText(_translate("Load", "<html><head/><body><p><span style=\" font-size:11pt; font-weight:600; color:#00007f;\">truant</span></p></body></html>"))
        self.lb_version.setText(_translate("Load", "<html><head/><body><p><span style=\" font-size:11pt; font-weight:600; color:#00007f;\">0.4</span></p></body></html>"))
        self.label.setText(_translate("Load", "Укажите данные для подключения к серверу MySQL"))
        self.tb_file.setText(_translate("Load", "..."))

